https://www.openshift.com/blog/how-to-enable-static-egress-ip-in-ocp


[21-02-12|8:41:03]~/demo/egress>oc get hostsubnet
NAME                              HOST                              HOST IP         SUBNET          EGRESS CIDRS   EGRESS IPS
infra1.openshift.rhcasalab.com    infra1.openshift.rhcasalab.com    192.168.10.31   10.129.2.0/23
infra2.openshift.rhcasalab.com    infra2.openshift.rhcasalab.com    192.168.10.32   10.131.0.0/23
infra3.openshift.rhcasalab.com    infra3.openshift.rhcasalab.com    192.168.10.33   10.128.2.0/23



